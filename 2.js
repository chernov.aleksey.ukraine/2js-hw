//1. Описать своими словами в несколько строчек, зачем в программировании нужны циклы.
//
//Циклы позволяют выполнять один и тот же кусок кода много раз. При этом,
//во - первых: мы управляем повторением итераций и завтавляем код повторяться,
//пока выполняется (или наступит событие, когда выполнится) определенное условие.
//во - вторых: мы управляем процессом изменения значений конкретных переменных и можем использовать,
//эти значения, в том числе, за пределами данного цикла.
//
//
 
// EX.1
console.log('Now we are looking for numbers which are multiples for 5.');
let a = +prompt('Now we are looking for numbers which are multiples for 5. Enter the number');
while (isNaN(a) || Math.trunc(a) < a || a <= 0) {
    a = +prompt('There was a mistake. Re-enter the number'); 
}
if (a <= 4) {
    console.log('Sorry, no numbers');
} else for (let i = 5; i <=  a; i = i + 5) {
    console.log(i); 
}

// EX.2 (advanced)
console.log('Now we are looking for all prime numbers in the range from m to n.');
let m = +prompt('Enter the first number "m"');
let n = +prompt('Enter the second number "n"');
while (isNaN(m) || Math.trunc(m) < m || m <= 0 || isNaN(n) || Math.trunc(n) < n || n <= 0 || n <= m) {
    alert("There was a mistake. You should enter both numbers again!");
    m = +prompt('Re-enter the first number "m"');
    n = +prompt('Re-enter the second number "n"');
}
let primeCheck = 0;
nextCheck: for (let i = m; i <= n; i++) {
    for (let j = 2; j < i; j++) {if (i % j === 0) continue nextCheck;}
    console.log(i);
    primeCheck = ++primeCheck
    }
if (primeCheck === 0) {
    console.log("Sorry. There are no any prime numbers in the range from " + m + " to " + n);
    alert (`Sorry. There are no any prime numbers in the range from ${m} to ${n} . `);
}